#ifndef FILEDIALOG_H
#define FILEDIALOG_H

#include <QDialog>
#include "addace.h"

extern QString NglobalFilePath;


namespace Ui {
class fileDialog;
}

class fileDialog : public QDialog
{
    Q_OBJECT

public slots:
    void secondSlot(QString FilePath);

public:
    void SetSettings();
    void PrintFileInfo();
    void setLabel(const QString& FilePath);
    QString fileDialog::getOwnName();
    explicit fileDialog(QWidget *parent = nullptr);
    ~fileDialog();

private slots:
    void cellDoubleClicked();
    void DeleteAceFunc();
    void ChangeStatus();
    void CleanTable();
    //void PrintTable();
    //void on_ReadFileName_clicked();
    //void on_AddAceButton_clicked();

    void on_pushButton_clicked();

private:
    Ui::fileDialog *ui;

};

#endif // FILEDIALOG_H
