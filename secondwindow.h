#ifndef SECONDWINDOW_H
#define SECONDWINDOW_H

#include <QDialog>
#include <QTableWidget>
#include "windows.h"
#include <tlhelp32.h>
#include <winbase.h>
#include <processthreadsapi.h>

namespace Ui {
class SecondWindow;
}

class SecondWindow : public QDialog
{
    Q_OBJECT

public:
    DWORD ProcessIDForChangePrivileges;
    explicit SecondWindow(QWidget *parent = nullptr);
    ~SecondWindow();

private slots:
    void AddItem(QTableWidget *tableWidget, QString QtStr, int column);
    void on_tableWidget_cellDoubleClicked(int row, int column);

private:
    Ui::SecondWindow *ui;
};

#endif // SECONDWINDOW_H
