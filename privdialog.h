#ifndef PRIVDIALOG_H
#define PRIVDIALOG_H

#include <QDialog>
#include <QTableWidget>
#include <QDir>
#include "windows.h"

namespace Ui {
class PrivDialog;
}

class PrivDialog : public QDialog
{
    Q_OBJECT

public:
    //DWORD ProcessIDForChangePrivileges;
    PrivDialog(QWidget *parent, int ProcessIDForChangePrivileges);
    void change(int ProcIDforDoubleCliecked);
    BOOL SetPrivilege(HANDLE hToken, LPCTSTR lpszPrivilege, BOOL bEnablePrivilege );
    void PrintPrivileges(int ProcessIDForChangePrivileges);
    explicit PrivDialog(QWidget *parent = nullptr);
    ~PrivDialog();

private slots:
    void on_tableWidget_cellDoubleClicked(int row, int column);

private:
    Ui::PrivDialog *ui;
};

#endif // PRIVDIALOG_H
