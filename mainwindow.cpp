#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "privdialog.h"
#include "filedialog.h"
#include <QMessageBox>
#include <QDialog>
#include <QString>
#include <QByteArray>
#include <QTableWidget>
#include <QEvent>
#include <QTableWidgetItem>
#include <QTimer>
#include <QTime>
#include <QDir>
#include <QInputDialog>
#include <QFileDialog>
#include "windows.h"
#include "winnt.h"
#include <tlhelp32.h>
#include <psapi.h>
#include <fileapi.h>
#include <processthreadsapi.h>
#include <wow64apiset.h>
#include <sddl.h>
#include <winbase.h>
#include <vector>
#include "aclapi.h"
#pragma comment(lib, "advapi32.lib")

using namespace jsonxx;
using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    filedialog = new fileDialog(this);
    QWidget::setWindowTitle("Процессы в системе");
    connect(ui->tableWidget, &QTableWidget::cellDoubleClicked, this, &MainWindow::cellDoubleClicked);
    connect(this, &MainWindow::secondSignal, filedialog, &fileDialog::secondSlot);
    //PrintProcessList();
}

void MainWindow::slotTimer()
{
    ui->tableWidget->clear();
    int row = ui->tableWidget->rowCount();
    for (int i = 0; i < row; i++)
    ui->tableWidget->removeRow(i); //удаляем все строки
    ui->tableWidget->setRowCount(0); //удаление таблицы без заголовков
    //PrintProcessList();
}

void MainWindow::SetSettings()
{
        ui->tableWidget->setColumnCount(15); //15 столбцов
        QStringList name_table;
        name_table << "Name of process" << "PID" << "File path" << "PPID" << "Name of User" << "SID" << "32-bit/64-bit" << "Native code" << "DEP" << "ASLR" << "DLL" << "Priority" << "Memory" << "Integrity level" << "Number of privileges";
        ui->tableWidget->setHorizontalHeaderLabels(name_table);
}



void MainWindow::cellDoubleClicked(int row, int column)
    {
        char *end;
        QString check = ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text();
        QByteArray text = check.toStdString().data();
        const char *chStr = text.constData();
        //ID = strtol(chStr, &end, 16);
        //ProcID = strtol(chStr, &end, 16);
        //IDProc = strtol(chStr, &end, 16);
        //Integrity Level
        if (ui->tableWidget->currentColumn() == 13)
        {
           QString ProcName = ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->text();
           QString SelectedValue = QInputDialog::getItem(this, "Change IL", "Change", {"LOW", "MEDIUM", "HIGH", "SYSTEM"});
           ui-> tableWidget->setItem(ui->tableWidget->currentRow(), ui->tableWidget->currentColumn(), new QTableWidgetItem(SelectedValue));
           system((QDir::currentPath() + QString::fromStdString("/somethingwithspaces/Project1.exe ") + SelectedValue + QString::fromStdString(" intglvl ") + ProcName + " 0 0 0").toLatin1());
           cout << "/somethingwithspaces/Project1.exe " + SelectedValue.toStdString() + " intglvl" + ProcName.toStdString() + " 0 0 0";
           //cout << SelectedValue.toStdString() << ProcName.toStdString() << endl;
           //ui->tableWidget->setTextElideMode()
        }
        else if (ui->tableWidget->currentColumn() == 14)
        {

            QString PID = ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text();
            PrivDialog new_window(this, PID.toInt(nullptr, 16));
            new_window.setModal(true);
            new_window.exec();

        }
        else
        {
            //mod = new Module(this);
            //mod->show();
        }
}

/*Name of process - 0
 PID - 1
 File path - 2
 PPID - 3
 Name of User - 4
 SID - 5
 32-bit/64-bit - 6
 Native code - 7
 DEP - 8
 ASLR - 9
 DLL - 10
 Priority - 11
 Memory - 12
 Integrity level - 13
 Privileges - 14
 */


void MainWindow::PrintProcessList()
{
    std::cout << QDir::currentPath().toStdString() << std::endl;
   SetSettings();
   ifstream input("procs.json");
   assert(input.is_open());
   Object o;
   o.parse(input);
   std::cout << o << std::endl;
   //assert(o.has<Array>("params"));

   Array params = o.get<Array>("params");
   //cout << params << endl;
   //ui->tableWidget->insertRow(ui->tableWidget->rowCount());
   //ui->tableWidget->insertRow(ui->tableWidget->rowCount());
   //PID
   //ui->tableWidget->setRowCount(params.size());
   //
   //std::cout << params.size() << std::endl;
   for(int i = 0; i < params.size(); i++){
       Object Process_param = params.get<Object>(i);
       ui->tableWidget->insertRow(ui->tableWidget->rowCount());
       /*jh cgfstatic const char* fields[15] = {
           "Process name", "PID", "FilePath", "PPID", "Domain", "SID", "System type", "Native code", "DEP",
           "ASLR", "dlls", "Priority", "Memory", "Integrity level", "Privileges"
       };
       for (int j = 0; j < 15; ++j) {
           //std::cout << fields[j] << std::endl;
           if (Process_param.has<String>(fields[j])) {
                QString v = QString::fromStdString(Process_param.get<String>(fields[j]));
                ui->tableWidget->setItem(i, j, new QTableWidgetItem(v));
           }
       }*/

       //Process name
       QString v = QString::fromStdString(Process_param.get<String>("Process name"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 0, new QTableWidgetItem(v));
       //PID
       v = QString::fromStdString(Process_param.get<String>("PID"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, new QTableWidgetItem(v)); //Устанавливает элемент для данной строки и столбца как элемент
       //File Path
       v = QString::fromStdString(Process_param.get<String>("FilePath"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 2, new QTableWidgetItem(v));
       //cout << v.toStdString() << endl;
       //PPID
       v = QString::fromStdString(Process_param.get<String>("PPID"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 3, new QTableWidgetItem(v));
       //Name of User
       v = QString::fromStdString(Process_param.get<String>("Domain"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 4, new QTableWidgetItem(v));
       //SID
       v = QString::fromStdString(Process_param.get<String>("SID"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 5, new QTableWidgetItem(v));
       //32-bit/64-bit
       v = QString::fromStdString(Process_param.get<String>("System type"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 6, new QTableWidgetItem(v));
       //Native code
       v = QString::fromStdString(Process_param.get<String>("Native code"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 7, new QTableWidgetItem(v));
       //DEP
       v = QString::fromStdString(Process_param.get<String>("DEP"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 8, new QTableWidgetItem(v));
       //ASLR
       v = QString::fromStdString(Process_param.get<String>("ASLR"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 9, new QTableWidgetItem(v));
       //DLL
       v = QString::fromStdString(Process_param.get<String>("Libraries"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 10, new QTableWidgetItem(v));
       //Priority
       v = QString::fromStdString(Process_param.get<String>("Priority"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 11, new QTableWidgetItem(v));
       //Memory
       v = QString::fromStdString(Process_param.get<String>("Memory"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 12, new QTableWidgetItem(v));
       //Integrity level
       v = QString::fromStdString(Process_param.get<String>("Integrity level"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 13, new QTableWidgetItem(v));
       //Privileges
       v = QString::fromStdString(Process_param.get<String>("Privileges"));
       ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 14, new QTableWidgetItem(v));
       //AddItem(ui->tableWidget, v, 13);
       //ui->tableWidget->resizeColumnsToContents();
       //ui->tableWidget->insertRow(ui->tableWidget->rowCount());
   }

}


void MainWindow::change_privileges()
{
    HANDLE curHandle, hToken;
    int row=ui->tableWidget->currentRow();
    QTableWidgetItem *tmp = ui->tableWidget->item(row, 0);
    QString modifPril= tmp->text();//privil_name
    QString modP = QString("%1").arg(modifPril);
    LPCWSTR my_Priv = reinterpret_cast<LPCWSTR>(modP.utf16());

    QTableWidgetItem *currentPriv = ui->tableWidget->item(row, 1);
    QString curPriv= currentPriv->text();//privil_of_on
    BOOL on_off;
    if (curPriv=="ENABLED"){on_off=FALSE;}
    else if (curPriv=="DISABLED"){on_off=TRUE;}
    else {return;}
    //curHandle = OpenProcess(PROCESS_ALL_ACCESS, false, ProcIDFor);
    OpenProcessToken(curHandle, TOKEN_QUERY|TOKEN_ADJUST_PRIVILEGES, &hToken);
    char hT[100]; //про запас
    sprintf(hT, "%p", hToken); // /// //передать в экзешник hT
    //SetPrivilege(hT, my_Priv, on_off); ////вот тут вызывается экзешник с этими же параметрами
    CloseHandle(hToken);
    CloseHandle(curHandle);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionUpdate_the_table_triggered()
{
    system((QDir::currentPath() + QString::fromStdString("/somethingwithspaces/Project1.exe 0 0 0 0 0 0")).toLatin1());
    PrintProcessList();
}

void MainWindow::on_actionShow_Files_Info_triggered()
{
    QString file_name = QFileDialog::getOpenFileName(this, "Open a file", QDir::homePath());
    cout << file_name.toStdString();
    system((QDir::currentPath() + QString::fromStdString("/somethingwithspaces2/FileInfo.exe ") + file_name + QString::fromStdString(" 0 ")).toLatin1());
    //system((QDir::currentPath().toStdString() + "/somethingwithspaces2/FileInfo.exe " + file_name.toStdString() + QString::fromStdString("0"));
    fileDialog new_dialog(this);
    new_dialog.setModal(true);
    new_dialog.exec();

}
