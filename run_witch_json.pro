QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    addace.cpp \
    filedialog.cpp \
    fileusers.cpp \
    jsonxx.cc \
    main.cpp \
    mainwindow.cpp \
    privdialog.cpp

HEADERS += \
    addace.h \
    filedialog.h \
    fileusers.h \
    jsonxx.h \
    mainwindow.h \
    privdialog.h

FORMS += \
    addace.ui \
    filedialog.ui \
    fileusers.ui \
    mainwindow.ui \
    privdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
