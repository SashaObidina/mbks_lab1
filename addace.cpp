#include "addace.h"
#include "ui_addace.h"
#include "sddl.h"
#include <aclapi.h>
#include "filedialog.h"
#include "ui_filedialog.h"
#include "jsonxx.h"
#include "addace.h"
#include <QMainWindow>
#include <QTableWidget>
#include <QTableWidgetItem>
#include "windows.h"
#include <tlhelp32.h>
#include <winbase.h>
#include <processthreadsapi.h>
#include <iostream>
#include <fstream>

#include "windows.h"
#include "winnt.h"
#include <stdio.h>
#include <tlhelp32.h>
#include <psapi.h>
#include <fileapi.h>
#include <processthreadsapi.h>
#include <wow64apiset.h>
#include <sddl.h>
#include <winbase.h>
#include "aclapi.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <filesystem>
#include <experimental/filesystem>
#include <io.h>

#include <QMessageBox>
#include <QDialog>
#include <QString>
#include <QByteArray>
#include <QEvent>
#include <QDir>
#include <QInputDialog>
#include <QFileDialog>
#include <PrivDialog.h>
#include "winnt.h"
#include <psapi.h>
#include <fileapi.h>
#include <wow64apiset.h>
#include <sddl.h>
#include <vector>
#include "aclapi.h"
#include <QDebug>

using namespace jsonxx;
using namespace std;

QString NglobalFilePath;

AddAce::AddAce(QWidget *parent) :
QDialog(parent),
ui(new Ui::AddAce)
{
ui->setupUi(this);
QWidget::setWindowTitle("Files");
}

AddAce::~AddAce()
{
delete ui;
}

void AddAce::on_buttonOk_clicked()
{
    QString domainNameStr(ui->lineDomainName->text());
    QString userNameStr(ui->lineUserName->text());
    QString rightStr(ui->lineRight->text());
    QString allowedStr(ui->lineAllowed->text());
    QString fullUserNameStr = QString("%1\\%2").arg(domainNameStr).arg(userNameStr);

    PACL pAcl = 0, pNewDACL = 0;
    PSID aceSid, bufAceSid;
    ACCESS_ALLOWED_ACE* pAce;
    ACL_REVISION_INFORMATION FileAcl;
    LPCWSTR FileNameLPC = reinterpret_cast<LPCWSTR>(NglobalFilePath.utf16());


    DWORD Error = GetNamedSecurityInfo(FileNameLPC, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION | PROTECTED_DACL_SECURITY_INFORMATION,
    0, 0, &pAcl, 0, 0);
    if (Error != ERROR_SUCCESS)
    {
    QMessageBox *msg = new QMessageBox;
    msg->setText("ERROR: incorrect name!");
    msg->exec();
    return;
    }

    PSID lpSid = 0;
    LPCWSTR lpUserName = reinterpret_cast<LPCWSTR>(fullUserNameStr.utf16());

    DWORD dwAccessRight;
    LPCWSTR userNameLPC = reinterpret_cast<LPCWSTR>(userNameStr.utf16());
    EXPLICIT_ACCESS ea;
    PSID pEveryoneSID = 0;
    SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;
    if (rightStr == "delete") dwAccessRight = DELETE;
    else if (rightStr == "write_owner") dwAccessRight = WRITE_OWNER;
    else if (rightStr == "write_DAC") dwAccessRight = WRITE_DAC;
    else if (rightStr == "read") dwAccessRight = FILE_GENERIC_READ;
    else if (rightStr == "write") dwAccessRight = FILE_GENERIC_WRITE;
    //else if (rightStr == "execute") dwAccessRight = FILE_GENERIC_EXECUTE;
    else if (rightStr == "synchronize") dwAccessRight = SYNCHRONIZE;
    else if (rightStr == "read_control") dwAccessRight = READ_CONTROL;
    else if (rightStr == "all_access") dwAccessRight = FILE_ALL_ACCESS;
    else if (rightStr == "maximum_allowed") dwAccessRight = MAXIMUM_ALLOWED;
    else if (rightStr == "generic_read") dwAccessRight = GENERIC_READ;
    else if (rightStr == "generic_write") dwAccessRight = GENERIC_WRITE;
    else if (rightStr == "generic_execute") dwAccessRight = GENERIC_EXECUTE;
    else if (rightStr == "generic_all") dwAccessRight = GENERIC_ALL;
    else
    {
    perror("ChangeStatus Right Error");
    QMessageBox *msg = new QMessageBox;
    msg->setText("ERROR: incorrect right!");
    msg->exec();
    if (pEveryoneSID != 0)
    LocalFree(pEveryoneSID);
    return;
    }

    if (!AllocateAndInitializeSid(&SIDAuthWorld, 1, SECURITY_WORLD_RID, 0, 0, 0, 0, 0, 0, 0, &pEveryoneSID))
    {
    printf("ChangeStatus InitializeSid Error %u\n", GetLastError());
    QMessageBox *msg = new QMessageBox;
    msg->setText("ERROR (1)!");
    msg->exec();
    if (pEveryoneSID != 0)
    LocalFree(pEveryoneSID);
    return;
    }

    // Create a new ACL that merges the new ACE into the existing DACL.
    ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS));

    if (allowedStr == "0")

    BuildExplicitAccessWithNameW(&ea, (LPWSTR)lpUserName, dwAccessRight, GRANT_ACCESS, NO_INHERITANCE);
    else if (allowedStr == "1")
    BuildExplicitAccessWithNameW(&ea, (LPWSTR)lpUserName, dwAccessRight, DENY_ACCESS, NO_INHERITANCE);
    else
    {
    QMessageBox *msg = new QMessageBox;
    msg->setText("ERROR: incorrect status!");
    msg->exec();
    if (pEveryoneSID != 0)
    LocalFree(pEveryoneSID);
    return;
    }
    Error = SetEntriesInAcl(1, &ea, pAcl, &pNewDACL);
    if (ERROR_SUCCESS != Error)
    {
    printf("ChangeStatus SetEntries Error %u\n", GetLastError());
    QMessageBox *msg = new QMessageBox;
    msg->setText("ERROR: incorrect name!");
    msg->exec();
    if (pNewDACL != 0)
    LocalFree((HLOCAL)pNewDACL);
    if (pEveryoneSID != 0)
    LocalFree(pEveryoneSID);
    return;
    }

    // Attach the new ACL as the object's DACL.
    Error = SetNamedSecurityInfo((LPWSTR)FileNameLPC, SE_FILE_OBJECT,
    DACL_SECURITY_INFORMATION | PROTECTED_DACL_SECURITY_INFORMATION,
    0, 0, pNewDACL, 0);
    if (ERROR_SUCCESS != Error)
    {
    printf("ChangeStatus SetSecurity Error %u\n", GetLastError());
    QMessageBox *msg = new QMessageBox;
    msg->setText("ERROR (3)!");
    msg->exec();
    if (pNewDACL != 0)
    LocalFree((HLOCAL)pNewDACL);
    if (pEveryoneSID != 0)
    LocalFree(pEveryoneSID);
    return;
    }

    if (pNewDACL != 0)
    LocalFree((HLOCAL)pNewDACL);
    if (pEveryoneSID != 0)
    LocalFree(pEveryoneSID);

    this->close();
}
