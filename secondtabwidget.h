#ifndef SECONDTABWIDGET_H
#define SECONDTABWIDGET_H

#include <QTabWidget>

namespace Ui {
class secondTabWidget;
}

class secondTabWidget : public QTabWidget
{
    Q_OBJECT

public:
    explicit secondTabWidget(QWidget *parent = nullptr);
    ~secondTabWidget();

private:
    Ui::secondTabWidget *ui;
};

#endif // SECONDTABWIDGET_H
