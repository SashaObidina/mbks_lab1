#include "sddl.h"
#include <aclapi.h>
#include "filedialog.h"
#include "ui_filedialog.h"
#include "jsonxx.h"
#include <QMainWindow>
#include <QTableWidget>
#include <QTableWidgetItem>
#include "windows.h"
#include <tlhelp32.h>
#include <winbase.h>
#include <processthreadsapi.h>
#include <iostream>
#include <fstream>

#include "windows.h"
#include "winnt.h"
#include <stdio.h>
#include <tlhelp32.h>
#include <psapi.h>
#include <fileapi.h>
#include <processthreadsapi.h>
#include <wow64apiset.h>
#include <sddl.h>
#include <winbase.h>
#include "aclapi.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <filesystem>
#include <experimental/filesystem>
#include <io.h>

#include <QMessageBox>
#include <QDialog>
#include <QString>
#include <QByteArray>
#include <QEvent>
#include <QDir>
#include <QInputDialog>
#include <QFileDialog>
#include <PrivDialog.h>
#include "winnt.h"
#include <psapi.h>
#include <fileapi.h>
#include <wow64apiset.h>
#include <sddl.h>
#include <vector>
#include "aclapi.h"

using namespace jsonxx;
using namespace std;

QString globalFilePath;


fileDialog::fileDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::fileDialog)
{
    ui->setupUi(this);
    QWidget::setWindowTitle("FileInfo");
    connect(ui->tableWidget, &QTableWidget::cellDoubleClicked, this, &fileDialog::cellDoubleClicked);
    PrintFileInfo();
}

fileDialog::~fileDialog()
{
    delete ui;
}


void fileDialog::secondSlot(QString FilePath)
{

    globalFilePath=QString("%1").arg(FilePath);
}

void fileDialog::SetSettings()
{
        ui->tableWidget->setColumnCount(7); //7 столбцов
        QStringList name_table;
        name_table << "File owner SID" << "File owner name" << "File path" << "Integrity level" << "User's access" << "User's names" << "User's rights";
        ui->tableWidget->setHorizontalHeaderLabels(name_table);
}

QString fileDialog::getOwnName()
{
    QFileInfo FileInfo(globalFilePath);
    QString FileNameTemp = QString("%1").arg(globalFilePath);
    LPCWSTR FileNameLPC = reinterpret_cast<LPCWSTR>(FileNameTemp.utf16());

    // РёС‰РµРј СѓСЂРѕРІРµРЅСЊ С†РµР»РѕСЃС‚РЅРѕСЃС‚Рё
    PSID lpSid = 0;
    PACL pDACL = 0, pSACL = 0;
    PSECURITY_DESCRIPTOR pSD = 0;
    LPSTR SIDParam;
    SYSTEM_MANDATORY_LABEL_ACE *sAce;
    DWORD dwIntegrityLevel;
    if (ERROR_SUCCESS != GetNamedSecurityInfo(FileNameLPC, SE_FILE_OBJECT, LABEL_SECURITY_INFORMATION,
        &lpSid,                 // Sid РІР»Р°РґРµР»СЊС†Р°
        0,                  // Sid РіСЂСѓРїРїС‹
        0, &pSACL,   // СЃРїРёСЃРєРё РїСЂР°РІ РґРѕСЃС‚СѓРїР°
        &pSD))
    {
        printf("Sacl info error %u\n", GetLastError());
    }
    else
    {
        if (0 != pSACL && pSACL->AceCount > 0)
        {
            GetAce(pSACL, 0, reinterpret_cast<void**>(&sAce));
            SID* sid = reinterpret_cast<SID*>(&sAce->SidStart);

        }
    }
    PWSTR stringSD;
    ULONG stringSDLen = 0;

    // РёС‰РµРј SID РІР»Р°РґРµР»СЊС†Р° С„Р°Р№Р»Р°
    DWORD Error = GetNamedSecurityInfo(FileNameLPC, SE_FILE_OBJECT, OWNER_SECURITY_INFORMATION | DACL_SECURITY_INFORMATION | PROTECTED_DACL_SECURITY_INFORMATION,
        &lpSid,                 // Sid РІР»Р°РґРµР»СЊС†Р°
        0,                  // Sid РіСЂСѓРїРїС‹
        &pDACL, 0,   // СЃРїРёСЃРєРё РїСЂР°РІ РґРѕСЃС‚СѓРїР°
        &pSD);

    // РёС‰РµРј РёРјСЏ РІР»Р°РґРµР»СЊС†Р° С„Р°Р№Р»Р°
    TCHAR lpName[256];
    TCHAR lpDomain[256];
    DWORD lpNameBuf = 256;
    SID_NAME_USE SidType;
    memset(lpName, 0, sizeof(lpName));
    memset(lpDomain, 0, sizeof(lpDomain));
    if (!LookupAccountSid(0, lpSid, lpName, &lpNameBuf, lpDomain, &lpNameBuf, &SidType))
        printf("Account Name Error: %d\n", GetLastError());

    // РёС‰РµРј СЃРїРёСЃРѕРє ACL
    ACL_SIZE_INFORMATION FileAcl;
    PSID pAceSid;
    BYTE AType;
    QString newLpName;
    //void *ace;

    GetAclInformation(pDACL, &FileAcl, sizeof(FileAcl), AclSizeInformation);

    for (DWORD i = 0; i < FileAcl.AceCount; i++)
    {
        bool flag = false;
        ACCESS_ALLOWED_ACE *pAce;
        if (!GetAce(pDACL, i, (LPVOID *)&pAce))
        {
            printf("First Ace Error(2) %u\n", GetLastError());
            return 0;
        }

        memset(lpName, 0, sizeof(lpName));
        memset(lpDomain, 0, sizeof(lpDomain));
        lpNameBuf = 256;
        pAceSid = &pAce->SidStart;
        if (!LookupAccountSid(0, pAceSid, lpName, &lpNameBuf, lpDomain, &lpNameBuf, &SidType))
        {
            QMessageBox *msg = new QMessageBox;
            msg->setText("2!");
            msg->exec();
            printf("Account Name Error(2): %d\n", GetLastError());
        }
        newLpName = QString("%1\\%2").arg(QString::fromWCharArray(lpDomain)).arg(QString::fromWCharArray(lpName));
        ui->tableWidget->setItem(i, 5, new QTableWidgetItem(newLpName));
        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    }
}

void fileDialog::PrintFileInfo()
{
   ui->tableWidget->insertRow(ui->tableWidget->rowCount());
   cout << QDir::currentPath().toStdString() << endl;
   SetSettings();
   ifstream input("files.json");
   assert(input.is_open());
   Object o;
   o.parse(input);

      //File owner SID
      QString v = QString::fromStdString(o.get<String>("File owner SID"));
      ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 0, new QTableWidgetItem(v));
      //File owner name
      v = QString::fromStdString(o.get<String>("File owner name"));
      ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, new QTableWidgetItem(v)); //Устанавливает элемент для данной строки и столбца как элемент
      

	/*QFileInfo FileInfo(globalFilePath);
      QString FileNameTemp = QString("%1").arg(globalFilePath);
      LPCWSTR FileNameLPC = reinterpret_cast<LPCWSTR>(FileNameTemp.utf16());

      // РёС‰РµРј СѓСЂРѕРІРµРЅСЊ С†РµР»РѕСЃС‚РЅРѕСЃС‚Рё
      PSID lpSid = 0;
      PACL pDACL = 0, pSACL = 0;
      PSECURITY_DESCRIPTOR pSD = 0;
      LPWSTR SIDParam;
      SYSTEM_MANDATORY_LABEL_ACE *sAce;
      DWORD dwIntegrityLevel;
      if (ERROR_SUCCESS != GetNamedSecurityInfo(FileNameLPC, SE_FILE_OBJECT, LABEL_SECURITY_INFORMATION,
          &lpSid,                 // Sid РІР»Р°РґРµР»СЊС†Р°
          0,                  // Sid РіСЂСѓРїРїС‹
          0, &pSACL,   // СЃРїРёСЃРєРё РїСЂР°РІ РґРѕСЃС‚СѓРїР°
          &pSD))
      {
          printf("Sacl info error %u\n", GetLastError());
      }
      else
      {
          if (0 != pSACL && pSACL->AceCount > 0)
          {
              GetAce(pSACL, 0, reinterpret_cast<void**>(&sAce));
              SID* sid = reinterpret_cast<SID*>(&sAce->SidStart);
              dwIntegrityLevel = sid->SubAuthority[0];
              printf("int_level %x\n", dwIntegrityLevel);
              //if (dwIntegrityLevel!=0)

                  if (dwIntegrityLevel == 0x0000) ;      //AddItem(ui->tableFile, "Untrusted", 2); // Low Integrity
                  else if (dwIntegrityLevel == 0x1000);       // AddItem(ui->tableFile, "Low level", 2);       // Low Integrity
                  else if (dwIntegrityLevel == 0x2000);       //AddItem(ui->tableFile, "Medium level", 2);    // Medium Integrity
                  else if (dwIntegrityLevel == 0x3000);      //AddItem(ui->tableFile, "High level", 2); // High Integrity
                  else if (dwIntegrityLevel == 0x4000);    // AddItem(ui->tableFile, "System level", 2); // System Integrity
                  else if (dwIntegrityLevel == 0x5000);   // AddItem(ui->tableFile, "Protected level", 2); // High Integrity
                  else     ;                                  // AddItem(ui->tableFile, "Untrusted", 2);

          }
          else
          {
              //printf("0 sacl %u\n", GetLastError());

          }
      }
      PWSTR stringSD;
      ULONG stringSDLen = 0;
      //ConvertSecurityDescriptorToStringSecurityDescriptor(pSD, SDDL_REVISION_1, LABEL_SECURITY_INFORMATION, &stringSD, &stringSDLen);

      lpSid = 0;
      if (pSD) LocalFree(pSD);

      // РёС‰РµРј SID РІР»Р°РґРµР»СЊС†Р° С„Р°Р№Р»Р°
      DWORD Error = GetNamedSecurityInfo(FileNameLPC, SE_FILE_OBJECT, OWNER_SECURITY_INFORMATION | DACL_SECURITY_INFORMATION | PROTECTED_DACL_SECURITY_INFORMATION,
          &lpSid,                 // Sid РІР»Р°РґРµР»СЊС†Р°
          0,                  // Sid РіСЂСѓРїРїС‹
          &pDACL, 0,   // СЃРїРёСЃРєРё РїСЂР°РІ РґРѕСЃС‚СѓРїР°
          &pSD);
      //ConvertSidToStringSid(lpSid, &SIDParam);

      // РёС‰РµРј РёРјСЏ РІР»Р°РґРµР»СЊС†Р° С„Р°Р№Р»Р°
      TCHAR lpName[256];
      QString lpN;
      TCHAR lpDomain[256];
      DWORD lpNameBuf = 256;
      SID_NAME_USE SidType;
      memset(lpName, 0, sizeof(lpName));
      memset(lpDomain, 0, sizeof(lpDomain));
      if (!LookupAccountSid(0, lpSid, lpName, &lpNameBuf, lpDomain, &lpNameBuf, &SidType))
          printf("Account Name Error: %d\n", GetLastError());
      else
      {
          lpN = QString("%1").arg(lpN);
          ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, new QTableWidgetItem(lpN));
          qDebug() << "Имя" << lpN;
      }*/

      //Integrity level
      v = QString::fromStdString(o.get<String>("Integrity level"));
      ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 3, new QTableWidgetItem(v));
      //File path
      v = QString::fromStdString(o.get<String>("File path"));
      ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 2, new QTableWidgetItem(v));
//       //cout << v.toStdString() << endl;
      




        Array params = o.get<Array>("User's access");
      for(int i = 0; i < params.size(); i++){
          String User_loc = params.get<String>(i);
          v = QString::fromStdString(User_loc);
          ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 4, new QTableWidgetItem(v));
          ui->tableWidget->insertRow(ui->tableWidget->rowCount());
      }
      ui->tableWidget->removeRow(ui->tableWidget->rowCount());

      Array params1 = o.get<Array>("User's names");
      for(int i = 0; i < params1.size(); i++){
          String User_loc = params1.get<String>(i);
          v = QString::fromStdString(User_loc);
          ui->tableWidget->setItem(i, 5, new QTableWidgetItem(v));
      }


      Array params2 = o.get<Array>("User's rights");
      for(int i = 0; i < params2.size(); i++){
          String User_loc = params2.get<String>(i);
          v = QString::fromStdString(User_loc);
          ui->tableWidget->setItem(i, 6, new QTableWidgetItem(v));
      }
      ui->tableWidget->removeRow(ui->tableWidget->rowCount());

}

void fileDialog::DeleteAceFunc()
{
    int row = ui->tableWidget->currentRow();
    int column = ui->tableWidget->currentColumn();

    PACL pAcl = 0;
    PSID aceSid;
    ACCESS_ALLOWED_ACE* pAce;
    ACL_REVISION_INFORMATION FileAcl;
    LPCWSTR FileNameLPC = reinterpret_cast<LPCWSTR>(globalFilePath.utf16());
   // perror("Delete Success\n");
    DWORD Error = GetNamedSecurityInfo(FileNameLPC, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION | PROTECTED_DACL_SECURITY_INFORMATION,
        0, 0, &pAcl, 0, 0);
    if (Error != ERROR_SUCCESS)
    {
       // perror("ERROR at getting information");
        return;
    }
    perror("Delete Success\n");
    if (!DeleteAce(pAcl, row))
    {
       // perror("DeleteAce");
        return;
    }
    perror("Delete Success\n");
    if (!SetNamedSecurityInfoW((LPWSTR)FileNameLPC, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION | PROTECTED_DACL_SECURITY_INFORMATION, 0, 0, pAcl, 0))
    {
        printf("Set security error %u\n", GetLastError());
    }

//    //system((QDir::currentPath().toStdString() + "/somethingwithspaces2/FilesInfo.exe " + file_name.toStdString()).c_str());
//    CleanTable();
//    PrintFileInfo();
}

void fileDialog::CleanTable()
{
    ui->tableWidget->clear();
    int row = ui->tableWidget->rowCount();
    for (int i = 0; i < row; i++)
        ui->tableWidget->removeRow(i);
    ui->tableWidget->setRowCount(0);
}

void fileDialog::ChangeStatus()
{
    int row = ui->tableWidget->currentRow();
    int column = ui->tableWidget->currentColumn();
    QString actualStatus = QString("%1").arg(ui->tableWidget->item(row, column)->text());
    //if (actualStatus == "denied") return;

    LPCWSTR FileNameLPC = reinterpret_cast<LPCWSTR>(globalFilePath.utf16());
    PACL pOldDACL = 0, pNewDACL = 0;
    // Get a pointer to the existing DACL.
    DWORD Error = GetNamedSecurityInfo(FileNameLPC, SE_FILE_OBJECT, DACL_SECURITY_INFORMATION | PROTECTED_DACL_SECURITY_INFORMATION,
        0, 0, &pOldDACL, 0, 0);
    if (Error != ERROR_SUCCESS)
    {
        printf("ChangeStatus SecurityInfo Error %u\n", GetLastError());
        return;
    }

    // Initialize an EXPLICIT_ACCESS structure for the new ACE.
    DWORD dwAccessRight;
    QString rightStr = QString("%1").arg(ui->tableWidget->item(row, column + 2)->text());
    QString userNameStr = QString("%1").arg(ui->tableWidget->item(row, column + 1)->text());
    LPCWSTR userNameLPC = reinterpret_cast<LPCWSTR>(userNameStr.utf16());
    ACCESS_ALLOWED_ACE *pAce;
    EXPLICIT_ACCESS ea;
    PSID pEveryoneSID = 0;
    SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;
    if (rightStr.contains("delete", Qt::CaseInsensitive))                       dwAccessRight = DELETE;
    else if (rightStr.contains("write_owner", Qt::CaseInsensitive))             dwAccessRight = WRITE_OWNER;
    else if (rightStr.contains("write_DAC", Qt::CaseInsensitive))               dwAccessRight = WRITE_DAC;
    else if (rightStr.contains("read", Qt::CaseInsensitive))                    dwAccessRight = FILE_GENERIC_READ;
    else if (rightStr.contains("write", Qt::CaseInsensitive))                   dwAccessRight = FILE_GENERIC_WRITE;
    else if (rightStr.contains("execute", Qt::CaseInsensitive))                 dwAccessRight = FILE_GENERIC_EXECUTE;
    else if (rightStr.contains("synchronize", Qt::CaseInsensitive))             dwAccessRight = SYNCHRONIZE;
    else if (rightStr.contains("read_control", Qt::CaseInsensitive))            dwAccessRight = READ_CONTROL;
    else if (rightStr.contains("all_access", Qt::CaseInsensitive))              dwAccessRight = FILE_ALL_ACCESS;
    else if (rightStr.contains("maximum_allowed", Qt::CaseInsensitive))         dwAccessRight = MAXIMUM_ALLOWED;
    else if (rightStr.contains("generic_read", Qt::CaseInsensitive))            dwAccessRight = GENERIC_READ;
    else if (rightStr.contains("generic_write", Qt::CaseInsensitive))           dwAccessRight = GENERIC_WRITE;
    else if (rightStr.contains("generic_execute", Qt::CaseInsensitive))         dwAccessRight = GENERIC_EXECUTE;
    else if (rightStr.contains("generic_all", Qt::CaseInsensitive))             dwAccessRight = GENERIC_ALL;
    else
    {
        perror("ChangeStatus Right Error");
        if (pEveryoneSID != 0)
            LocalFree(pEveryoneSID);
        return;
    }
    if (!GetAce(pOldDACL, row, (LPVOID *)&pAce))
    {
        perror("ChangeStatus GetAce Error");
        return;
    }
    if (!AllocateAndInitializeSid(&SIDAuthWorld, 1, SECURITY_WORLD_RID, 0, 0, 0, 0, 0, 0, 0, &pEveryoneSID))
    {
        printf("ChangeStatus InitializeSid Error %u\n", GetLastError());
        if (pEveryoneSID != 0)
            LocalFree(pEveryoneSID);
        return;
    }
    ZeroMemory(&ea, sizeof(EXPLICIT_ACCESS));

    // Create a new ACL that merges the new ACE into the existing DACL.
    if (actualStatus=="allowed")
    BuildExplicitAccessWithNameW(&ea, (LPWSTR)userNameLPC, dwAccessRight, DENY_ACCESS, NO_INHERITANCE);
    else if (actualStatus == "denied")
        BuildExplicitAccessWithNameW(&ea, (LPWSTR)userNameLPC, dwAccessRight, GRANT_ACCESS, NO_INHERITANCE);
    Error = SetEntriesInAcl(1, &ea, pOldDACL, &pNewDACL);
    if (ERROR_SUCCESS != Error)
    {
        printf("ChangeStatus SetEntries Error %u\n", GetLastError());
        if (pNewDACL != 0)
            LocalFree((HLOCAL)pNewDACL);
        if (pEveryoneSID != 0)
            LocalFree(pEveryoneSID);
        return;
    }

    // Attach the new ACL as the object's DACL.
    Error = SetNamedSecurityInfo((LPWSTR)FileNameLPC, SE_FILE_OBJECT,
        DACL_SECURITY_INFORMATION | PROTECTED_DACL_SECURITY_INFORMATION,
        0, 0, pNewDACL, 0);
    if (ERROR_SUCCESS != Error)
    {
        printf("ChangeStatus SetSecurity Error %u\n", GetLastError());
        if (pNewDACL != 0)
            LocalFree((HLOCAL)pNewDACL);
        if (pEveryoneSID != 0)
            LocalFree(pEveryoneSID);
        return;
    }

    if (pNewDACL != 0)
        LocalFree((HLOCAL)pNewDACL);
    if (pEveryoneSID != 0)
        LocalFree(pEveryoneSID);

    CleanTable();
    perror("ChangeStatus SetEntries Success");
    PrintFileInfo();
    perror("ChangeStatus SetEntries Success!");
}

void fileDialog::cellDoubleClicked()
{
    if (ui->tableWidget->currentColumn() == 3)
    {
       QString SelectedValue = QInputDialog::getItem(this, "Change IL", "Change", {"LOW", "MEDIUM", "HIGH", "SYSTEM"});
       ui-> tableWidget->setItem(ui->tableWidget->currentRow(), ui->tableWidget->currentColumn(), new QTableWidgetItem(SelectedValue));
       QString FilePath = ui->tableWidget->item(0, 2)->text();
       std::cout << FilePath << std::endl;
       system((QDir::currentPath() + QString::fromStdString("/somethingwithspaces2/FileInfo.exe ") + FilePath +" "+ SelectedValue).toLatin1());
    }
    else if (ui->tableWidget->currentColumn() == 1)
    {
        QString SelectedValue = QInputDialog::getItem(this, "Change Owner", "Change", {"Admin", "current owner"});
        ui-> tableWidget->setItem(ui->tableWidget->currentRow(), ui->tableWidget->currentColumn(), new QTableWidgetItem(SelectedValue));
        QString FilePath = ui->tableWidget->item(0, 2)->text();
        if(SelectedValue == QString::fromStdString("Admin")){
            system((QDir::currentPath() + QString::fromStdString("/somethingwithspaces2/FileInfo.exe ") + FilePath +" "+ QString::fromStdString("owner")).toLatin1());
             std::cout << FilePath.toStdString() << "owner" << std::endl;
        }
        if(SelectedValue == QString::fromStdString("current owner")){
            system((QDir::currentPath() + QString::fromStdString("/somethingwithspaces2/FileInfo.exe ") + FilePath +" "+ QString::fromStdString("cur")).toLatin1());
        }

    }
    int column = ui->tableWidget->currentColumn();


     if (column == 5 || column == 6) DeleteAceFunc();
     else if (column == 4) ChangeStatus();
}

void fileDialog::on_pushButton_clicked()
{
    class AddAce window;
    window.setModal(true);
    window.exec();
    CleanTable();
    PrintFileInfo();
}

