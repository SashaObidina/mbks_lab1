#include "windows.h"
#include "winnt.h"
#include <processthreadsapi.h>
#include <tlhelp32.h>
#include <psapi.h>
#include <fileapi.h>
#include <wow64apiset.h>
#include <sddl.h>
#include <winbase.h>
#include <vector>
#include "aclapi.h"

SecondWindow::SecondWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SecondWindow)
{
    ui->setupUi(this);
}

SecondWindow::~SecondWindow()
{
    delete ui;
}

void SecondWindow::AddItem(QTableWidget *tableWidget, QString QtStr, int column)
{
    tableWidget->setItem(tableWidget->rowCount() - 1, column, new QTableWidgetItem(QtStr)); //Устанавливает элемент для данной строки и столбца как элемент
    QTableWidgetItem *newItem = new QTableWidgetItem(); //Создание элемента таблицы указанного типа, который не принадлежит ни одной таблице
    newItem->setText(QtStr);
    tableWidget->setItem(tableWidget->rowCount(), column, newItem);
}

void SecondWindow::on_tableWidget_cellDoubleClicked(int row, int column)
{

        HANDLE hToken;
        DWORD dSize;
        PTOKEN_PRIVILEGES Priv;
        DWORD dwPrivilegeNameSize;
        TCHAR ucPrivilegeName[256];
        HANDLE curHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, false, ProcessIDForChangePrivileges);
        BOOL t=OpenProcessToken(curHandle, TOKEN_QUERY, &hToken);
        if (t==FALSE){return;}

        if (!GetTokenInformation(hToken, TokenPrivileges, NULL, 0, &dSize))
        {if (GetLastError() != ERROR_INSUFFICIENT_BUFFER)
        {
            printf("1PRIVGetTokenInformation Error %u\n", GetLastError());}
        }
        Priv= (PTOKEN_PRIVILEGES)GlobalAlloc(GPTR, dSize);
        if (!GetTokenInformation(hToken, TokenPrivileges, Priv, dSize, &dSize))
        {
            printf("2PRIV:GetTokenInformation Error %u\n", GetLastError());
        }
        for (DWORD i = 0; i < Priv->PrivilegeCount; i++)
        {
            dwPrivilegeNameSize = sizeof(ucPrivilegeName);
            LookupPrivilegeName(NULL, &Priv->Privileges[i].Luid,ucPrivilegeName, &dwPrivilegeNameSize);
            ui->tableWidget->insertRow(ui->tableWidget->rowCount());
            AddItem(ui->tableWidget, QString::fromWCharArray(ucPrivilegeName), 0);
            if (Priv->Privileges[i].Attributes == SE_PRIVILEGE_USED_FOR_ACCESS)
            {
                AddItem(ui->tableWidget, "USED_FOR_ACCESS", 1);
            }
            else if (Priv->Privileges[i].Attributes == SE_PRIVILEGE_ENABLED_BY_DEFAULT)
            {
                AddItem(ui->tableWidget,"ENABLED_BY_DEFAULT", 1);
            }
            else if (Priv->Privileges[i].Attributes == SE_PRIVILEGE_ENABLED)
            {
                AddItem(ui->tableWidget, "ENABLED", 1);
            }
            else if (Priv->Privileges[i].Attributes == SE_PRIVILEGE_REMOVED)
            {
                AddItem(ui->tableWidget, "REMOVED", 1);
            }
            else if (Priv->Privileges[i].Attributes == 0x00000003L)
            {
                AddItem(ui->tableWidget, "DEFAULT_ENABLED", 1);
            }
            else {
                AddItem(ui->tableWidget, "DISABLED", 1);
            }

        }
        if (Priv) GlobalFree(Priv);
        if (hToken){CloseHandle(hToken);}
        CloseHandle(curHandle);

}
