#include "privdialog.h"
#include "ui_privdialog.h"
#include <iostream>

int ProcIDforDoubleCliecked;

PrivDialog::PrivDialog(QWidget *parent, int ProcessIDForChangePrivileges) :
    QDialog(parent),
    ui(new Ui::PrivDialog)
{
    ui->setupUi(this);
    QWidget::setWindowTitle("Privileges");
    PrintPrivileges(ProcessIDForChangePrivileges);
}

PrivDialog::~PrivDialog()
{
    delete ui;
}


void PrivDialog::on_tableWidget_cellDoubleClicked(int row, int column)
{
    change(ProcIDforDoubleCliecked);
}

void PrivDialog::PrintPrivileges(int ProcessIDForChangePrivileges)
{
    ProcIDforDoubleCliecked = ProcessIDForChangePrivileges;
    HANDLE hToken;
    DWORD dSize;
    PTOKEN_PRIVILEGES Priv;
    DWORD dwPrivilegeNameSize;
    TCHAR ucPrivilegeName[256];
    //std::cout << ProcessIDForChangePrivileges << std::endl;
    HANDLE curHandle = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, false, ProcessIDForChangePrivileges);
    BOOL t=OpenProcessToken(curHandle, TOKEN_QUERY, &hToken);
    if (t==FALSE){return;}

    if (!GetTokenInformation(hToken, TokenPrivileges, NULL, 0, &dSize))
    {
        if (GetLastError() != ERROR_INSUFFICIENT_BUFFER) {
            printf("1PRIVGetTokenInformation Error %u\n", GetLastError());
        }
   }
    Priv= (PTOKEN_PRIVILEGES)GlobalAlloc(GPTR, dSize);
    if (!GetTokenInformation(hToken, TokenPrivileges, Priv, dSize, &dSize))
    {
        printf("2PRIV:GetTokenInformation Error %u\n", GetLastError());
    }
    ui->tableWidget->setColumnCount(2);
    //QString v = QString::fromStdString("USED_FOR_ACCESS");
    //ui->tableWidget->setItem(ui->tableWidget->rowCount() + 1, ui->tableWidget->columnCount(), new QTableWidgetItem(v));
    std::cout << ui->tableWidget->rowCount() << std::endl;

    for (DWORD i = 0; i < Priv->PrivilegeCount; i++)
    {
        //check = false;
        ui->tableWidget->insertRow(ui->tableWidget->rowCount());
        dwPrivilegeNameSize = sizeof(ucPrivilegeName);
        LookupPrivilegeName(NULL, &Priv->Privileges[i].Luid, ucPrivilegeName, &dwPrivilegeNameSize);
        ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 0, new QTableWidgetItem(QString::fromWCharArray(ucPrivilegeName)));


        if (Priv->Privileges[i].Attributes == SE_PRIVILEGE_USED_FOR_ACCESS)
        {
            QString v = QString::fromStdString("USED_FOR_ACCESS");
            ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, new QTableWidgetItem(v));
            //check = true;
        }

        if (Priv->Privileges[i].Attributes == SE_PRIVILEGE_ENABLED_BY_DEFAULT)
        {
            QString v = QString::fromStdString("ENABLED_BY_DEFAULT");
            ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, new QTableWidgetItem(v));

        }

        if (Priv->Privileges[i].Attributes == SE_PRIVILEGE_ENABLED)
        {
            QString v = QString::fromStdString("ENABLED");
            ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, new QTableWidgetItem(v));

        }

        if (Priv->Privileges[i].Attributes == SE_PRIVILEGE_REMOVED)
        {
            QString v = QString::fromStdString("REMOVED");
            ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, new QTableWidgetItem(v));

        }

        if (Priv->Privileges[i].Attributes == 0x00000003L)
        {
            QString v = QString::fromStdString("DEFAULT_ENABLED");
            ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, new QTableWidgetItem(v));

        }

        else {
           std::cout << "hggh" << std::endl;
           QString v = QString::fromStdString("DISABLED");
           ui->tableWidget->setItem(ui->tableWidget->rowCount() - 1, 1, new QTableWidgetItem(v));
           //AddItem(ui->tableWidget, "DISABLED", 1);
      }

    }
    if (Priv) GlobalFree(Priv);
    if (hToken){CloseHandle(hToken);}
    CloseHandle(curHandle);
}


void PrivDialog::change(int ProcIDforDoubleCliecked)
{
   HANDLE curHandle, hToken;
   int row=ui->tableWidget->currentRow();
   QTableWidgetItem *tmp = ui->tableWidget->item(row, 0);
   QString modifPril= tmp->text();//privil_name
   QString modP = QString("%1").arg(modifPril);
   LPCWSTR my_Priv = reinterpret_cast<LPCWSTR>(modP.utf16());

    QTableWidgetItem *currentPriv = ui->tableWidget->item(row, 1);
    QString curPriv= currentPriv->text();//privil_of_on
    BOOL on_off;
    if (curPriv=="ENABLED"){on_off=FALSE;}
    else if (curPriv=="DISABLED"){on_off=TRUE;}
    else {return;}
    curHandle = OpenProcess(PROCESS_ALL_ACCESS, false, ProcIDforDoubleCliecked);
    OpenProcessToken(curHandle, TOKEN_QUERY|TOKEN_ADJUST_PRIVILEGES, &hToken);
    char str[64]; //про запас
    sprintf(str, "%p", hToken);/////////////////
    SetPrivilege(hToken, my_Priv, on_off);
    CloseHandle(hToken);
    CloseHandle(curHandle);
}



BOOL PrivDialog:: SetPrivilege(
    HANDLE hToken,          // access token handle
    LPCTSTR lpszPrivilege,  // name of privilege to enable/disable
    BOOL bEnablePrivilege   // to enable or disable privilege
)
{
    TOKEN_PRIVILEGES tp;
    LUID luid;
    if (!LookupPrivilegeValue(
        NULL,            // lookup privilege on local system
        lpszPrivilege,   // privilege to lookup
        &luid))        // receives LUID of privilege
    {
        printf("LookupPrivilegeValue error: %u\n", GetLastError());
        return FALSE;
    }
    tp.PrivilegeCount = 1;
    tp.Privileges[0].Luid = luid;
    if (bEnablePrivilege)
        tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
    else
        tp.Privileges[0].Attributes = 0;

    // Enable the privilege or disable all privileges.
    if (!AdjustTokenPrivileges(
        hToken,
        FALSE,
        &tp,
        sizeof(TOKEN_PRIVILEGES),
        (PTOKEN_PRIVILEGES)NULL,
        (PDWORD)NULL))
    {
        printf("AdjustTokenPrivileges error: %u\n", GetLastError());
        return FALSE;
    }

    if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)
    {
       printf("The token does not have the specified privilege. \n");
        return FALSE;
    }
    return TRUE;
}

//void PrivDialog::cellDoubleClicked()
//{
//    change(ProcIDforDoubleCliecked);

//}


//void PrivDialog::change(int ProcessIDForChangePrivileges)
//{
//    HANDLE curHandle, hToken;
//    int row=ui->tableWidget->currentRow();
//    QTableWidgetItem *tmp = ui->tableWidget->item(row, 0);
//    QString modifPril= tmp->text();//privil_name
//    QString modP = modifPril; //QString("%1").arg(modifPril);
//    std::cout << row << " " << tmp->text().toStdString() << std::endl;
//    //LPCSTR my_Priv = reinterpret_cast<LPCSTR>(modP.utf16());

//    QTableWidgetItem *currentPriv = ui->tableWidget->item(row, 1);
//    QString curPriv= currentPriv->text();//privil_of_on
//    BOOL on_off;
//    if (curPriv=="ENABLED"){on_off=FALSE;}
//    else if (curPriv=="DISABLED"){on_off=TRUE;}
//    else {return;}
//    curHandle = OpenProcess(PROCESS_ALL_ACCESS, false, ProcessIDForChangePrivileges);
//    OpenProcessToken(curHandle, TOKEN_QUERY|TOKEN_ADJUST_PRIVILEGES, &hToken);
//    char hT[100]; //про запас
//    sprintf(hT, "%p", hToken); // /// //передать в экзешник hT
//    //system((QDir::currentPath().toStdString() + "/somethingwithspaces/Project1.exe").c_str());
//    QString s = QDir::currentPath() + QString::fromStdString("/somethingwithspaces/Project1.exe 0 ") + QString::fromStdString("privilg") + " 0 " + QString::fromUtf8(hT) + " " + modP + " " + QString::number(on_off);
//    std::cout << s.toStdString() << std::endl;
//    system((s).toLatin1());
//    //cout << "/somethingwithspaces/Project1.exe " + SelectedValue.toStdString() + " intglvl" + ProcName.toStdString() + " 0 0 0";
//    //SetPrivilege(hT, my_Priv, on_off); ////вот тут вызывается экзешник с этими же параметрами
//    CloseHandle(hToken);
//    CloseHandle(curHandle);
//}
