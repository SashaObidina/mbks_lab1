//#ifndef MAINWINDOW_H
//#define MAINWINDOW_H

//#include <QMainWindow>

//QT_BEGIN_NAMESPACE
//namespace Ui { class MainWindow; }
//QT_END_NAMESPACE

//class MainWindow : public QMainWindow
//{
//    Q_OBJECT

//public:
//    MainWindow(QWidget *parent = nullptr);
//    ~MainWindow();

//private slots:
//    void on_tableWidget_cellClicked(int row, int column);

//private:
//    Ui::MainWindow *ui;
//};
//#endif // MAINWINDOW_H


#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QTimer>
#include <QTime>
#include "windows.h"
#include <tlhelp32.h>
#include <winbase.h>
#include <processthreadsapi.h>
#include <iostream>
#include <fstream>
#include "jsonxx.h"
#include <filedialog.h>
extern DWORD ID;
extern DWORD ProcID;
extern QString ProcName;
extern DWORD IDProc;
extern QString str;
extern QString level;
extern QString globalFilePath;
extern QString FileNameGlobal;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    void SetSettings();
    void AddItem(QTableWidget *tableWidget, QString QtStr, int column);
    bool IsWow64(HANDLE hProcess, BOOL &isWow64);

    void IntegrLev(DWORD ID);
    int ModuleList(DWORD const dwProcessId);
public:

    void cellDoubleClicked(int row, int column);
    void Privileges();
    void change_privileges();
    void PrintProcessList();
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    //void cellDoubleClicked();

private slots:
    void slotTimer();
   // void on_pushButton_clicked();

    void on_actionUpdate_the_table_triggered();

    void on_actionShow_Files_Info_triggered();

signals:
    void secondSignal(QString FilePath);

private:
    Ui::MainWindow *ui;
   // Module *mod;
    QTimer *timer;
    fileDialog *filedialog {};
};
#endif // MAINWINDOW_H
