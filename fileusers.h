#ifndef FILEUSERS_H
#define FILEUSERS_H

#include <QDialog>

namespace Ui {
class FileUsers;
}

class FileUsers : public QDialog
{
    Q_OBJECT

public:
    explicit FileUsers(QWidget *parent = nullptr);
    ~FileUsers();

private:
    Ui::FileUsers *ui;
};

#endif // FILEUSERS_H
